function calculateTotal() {
    var total = 0.0;
    $("input[id^='forWhom-']").each(
        function (index, input) {
            if (parseFloat(input.value)) {
                total += parseFloat(input.value);
            }
        }
    );

    if ($('#addServiceCharge').prop('checked')) {
        total = total * 1.1;
    }

    $('#totalAmount').html(total.toFixed(2));
}

function addPayment() {
    var paymentData = new Object();
    paymentData.paidBy = $('#paidBy').val();
    paymentData.purpose = $('#purpose').val();


    var serviceChargeRate = 1.0;
    if ($('#addServiceCharge').prop('checked')) {
        serviceChargeRate = 1.1;
    }

    var paidForList = [];
    $("input[id^='forWhom-']").each(
        function (index, input) {
            if (input.value != "" && parseFloat(input.value) != 0) {
                paidForList.push({name: input.id.substr(8), amount: (input.value*serviceChargeRate)});
            }
        }
    );
    paymentData.paidFor = paidForList;

    $.ajax({
        async: false,
        type: 'POST',
        contentType: 'application/json',
        url: '/json/payment/add',
        data: JSON.stringify(paymentData),
        dataType: 'json',
        success: function() {
            self.location = "/balance/list";
        },
        error: function(data) {
            console.log("add failed. response = " + data);
        }
    });
}

function processUserList(data) {
    var source = $("#paid-for-template").html();
    var template = Handlebars.compile(source);

    var optionListHtml = '<option value="">Please select</option>';
    var paidForHtml = '';

    for (var i=0; i<data.length; i++) {
        optionListHtml+= '<option value="' + data[i].name + '">' + data[i].name + '</option>';
        paidForHtml += template(data[i]);
    }

    $('#paidBy').html(optionListHtml);
    $('#paidFor').html(paidForHtml);
}

$(function () {
    listUsers();
});