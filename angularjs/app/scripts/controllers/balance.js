'use strict';

angular.module('angularjsApp')
    .controller('BalanceCtrl', function ($scope, $http) {
        var url = '/json/balance/list';
        $http.get(url).success(function(data) {
            $scope.balances = data;
        });
    });
