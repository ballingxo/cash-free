package com.cashfree.gson;

import com.cashfree.entity.Payment;

public class GsonTest {
    public String json = "{\"paidBy\":\"111\",\"paidFor\":[{\"name\":\"Test\",\"amount\":\"1\"},{\"name\":\"111\",\"amount\":\"2\"},{\"name\":\"222\",\"amount\":\"3\"}]}";

    public void testPayment() throws Exception {
        Payment payment = GsonFactory.gson().fromJson(json, Payment.class);
        System.out.println("payment = " + payment);
    }

    public static void main(String[] args) throws Exception {
        new GsonTest().testPayment();
    }
}
