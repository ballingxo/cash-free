package com.cashfree.resource.json;

import com.cashfree.entity.User;
import com.cashfree.gson.GsonFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Path("/json/user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserJsonResource {

    @GET
    @Path("/list")
    public String list() {
        List<User> userList = ofy().load().type(User.class).order("name").list();
        String json = GsonFactory.gson().toJson(userList);
        return json;
    }

    @POST
    @Path("/add")
    public String add(String userStr) {
        User user = GsonFactory.gson().fromJson(userStr, User.class);
        System.out.println("Adding user " + user);
        ofy().save().entity(user).now();

        return JsonCommon.RESULT_OK;
    }
}
