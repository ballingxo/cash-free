package com.cashfree.resource.json;

import com.cashfree.entity.Balance;
import com.cashfree.entity.PaidFor;
import com.cashfree.entity.Payment;
import com.cashfree.gson.GsonFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Path("/json/balance")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BalanceJsonResource {

    @GET
    @Path("/list")
    public String list() {
        Map<String, Balance> balanceMap = new TreeMap<String, Balance>();
        List<Payment> paymentList = ofy().load().type(Payment.class).list();
        for (Payment payment : paymentList) {
            double totalPaid = 0;
            for (PaidFor paidFor : payment.getPaidFor()) {
                Balance paidForBalance = getBalance(balanceMap, paidFor.getName());
                double amount = paidFor.getAmount();
                paidForBalance.substract(amount);
                totalPaid += amount;
            }

            Balance paidByBalance = getBalance(balanceMap, payment.getPaidBy());
            paidByBalance.add(totalPaid);
        }

        List<Balance> balances = new ArrayList<Balance>(balanceMap.values());
        Collections.sort(balances);

        String json = GsonFactory.gson().toJson(balances);

        return json;
    }

    private Balance getBalance(Map<String, Balance> balanceMap, String name) {
        Balance balance = balanceMap.get(name);
        if (balance == null) {
            balance = new Balance(name);
            balanceMap.put(name, balance);
        }
        return balance;
    }
}

